'use strict';

let content = document.getElementById('content');

chrome.tabs.query({active: true, currentWindow: true}, function(tabs){
    let text = tabs[0].title;
    let jiraTicket = text.match(/[A-Z]+-[A-Z0-9]+/);
    let title = text.replace(/ *\[[^\]\[]*\]*/g, "")
    	.replace(" - JIRA", "")
    	.replace(/ /g, "-");
    title = (jiraTicket + title).toLowerCase();

    content.innerHTML = "feature/" + title 
    					+ "<br/>bug/" + title
    					+ "<br/><br/>"
    					+ "git checkout -b feature/" + title 
    					+ "<br/>git checkout -b bug/" + title;
});
