# Install
1. Clone the project
2. Open Chrome and got to `chrome://extensions/`
3. Tap `Load unpacked` and select the cloned repo

# How to use it
1. Open the Jira ticket in your browser
2. Tap the `Branch generator` extensions
3. It should display a window with branch names and git commands